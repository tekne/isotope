use std::collections::HashMap;
use super::{
    Value, ValueEnum,
    node::{Node, WeakNode},
    ty::TypeEnum,
    sexpr::Sexpr,
    RainError
};
use RainError::*;

/// A parameter to a lambda function
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Parameter {
    number: u32,
    func: WeakNode<Lambda>
}

impl Parameter {
    /// Get this parameter's number
    pub fn number(&self) -> u32 { self.number }
    /// Get the lambda this is a parameter of
    pub fn func(&self) -> WeakNode<Lambda> { self.func.clone() }
}

impl PartialEq<ValueEnum> for Parameter {
    fn eq(&self, other: &ValueEnum) -> bool {
        use ValueEnum::*;
        match other {
            Eval(e) => e.eq(&self.downcast()),
            Parameter(p) => self.eq(p),
            _ => false
        }
    }
}
impl From<Parameter> for ValueEnum {
    #[inline] fn from(p: Parameter) -> ValueEnum { ValueEnum::Parameter(p) }
}
impl Value for Parameter {
    #[inline] fn get_scope(&self) -> Result<WeakNode<Lambda>, ()> { Ok(self.func.clone()) }
    #[inline] fn substitute(&self, c: &mut SubstitutionContext)
    -> Result<Option<ValueEnum>, RainError> {
        if self.func != c.scope {
            if self.get_scope_level().map_err(|_| InvalidScope)? >= c.scope_level {
                return Err(ScopeTooDeep)
            } else {
                //TODO: stricter check?
                return Ok(None)
            }
        }
        c.args.get(self.number as usize).cloned().ok_or(ParamIxOutOfBounds).map(|v| Some(v))
    }
}


/// A lambda function
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Lambda {
    // The types of each parameter to this lambda function
    params: Vec<TypeEnum>,
    /// The result of this lambda function
    res: ValueEnum,
    /// The scope level of this lambda function
    scope_level: u32
}

impl Lambda {
    /// Get the types of the parameters to this lambda function
    #[inline] pub fn params(&self) -> &[TypeEnum] { &self.params }
}

#[derive(Debug, Clone)]
pub struct SubstitutionContext<'a> {
    pub args: &'a [ValueEnum],
    pub scope: WeakNode<Lambda>,
    pub scope_level: u32,
    pub cache: HashMap<WeakNode<Sexpr>, ValueEnum>,
    private: ()
}

impl Node<Lambda> {
    /// Get a parameter of this lambda function
    pub fn param(&self, n: u32) -> Result<Parameter, RainError> {
        match self.data().params.get(n as usize) {
            Some(_ty) => Ok(Parameter { number: n, func: self.downgrade() }),
            None => Err(InvalidParameter)
        }
    }
    /// Evaluate a lambda function on a vector of arguments
    pub fn evaluate(&self, args: &[ValueEnum]) -> Result<ValueEnum, RainError> {
        let data = self.data();
        let ps = data.params.len();
        if args.len() != ps { panic!("Expected {} arguments, got {}", ps, args.len()) }
        //TODO: type checking
        let mut sub_ctx = SubstitutionContext {
            args, scope: self.downgrade(), scope_level: data.scope_level, cache: HashMap::new(),
            private: ()
        };
        data.res.substitute(&mut sub_ctx).map(|v| v.unwrap_or_else(|| data.res.clone()))
    }
}

impl WeakNode<Lambda> {
    /// Get the scope level of this lambda function
    pub fn scope_level(&self) -> u32 {
        self.upgrade().map(|l| l.data().scope_level).unwrap_or(0)
    }
}

impl PartialEq<ValueEnum> for Node<Lambda> {
    fn eq(&self, other: &ValueEnum) -> bool {
        match other {
            ValueEnum::Eval(e) => e.eq(&self.downcast()),
            ValueEnum::Lambda(l) => l.eq(self),
            _ => false
        }
    }
}
impl From<Node<Lambda>> for ValueEnum {
    #[inline] fn from(n: Node<Lambda>) -> ValueEnum { ValueEnum::Lambda(n) }
}
impl Value for Node<Lambda> {}
