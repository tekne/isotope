pub mod node;
use node::{Node, WeakNode};
pub mod sexpr;
use sexpr::Eval;
pub mod lambda;
use lambda::{Lambda, Parameter, SubstitutionContext};
pub mod ty;
use ty::{Simple, TypeEnum, PrimitiveTy};
pub mod tuple;
use tuple::{Product, Tuple};
pub mod ops;
use ops::bitwise::*;
pub mod primitive;
use primitive::*;
pub mod projection;
use projection::Projection;

/// A `rain` value
#[derive(Debug, Clone)]
pub enum ValueEnum {
    /// A function evaluation
    Eval(Eval),
    /// A lambda function
    Lambda(Node<Lambda>),
    /// A parameter to a lambda function
    Parameter(Parameter),
    /// A projection of a `rain` value
    Proj(Projection),
    /// A simple type
    Simple(Simple),
    /// A tuple of values
    Tuple(Tuple),
    /// A product type
    Product(Product),
    /// A boolean primitive value
    Bool(bool),
    /// Boolean `and`
    And(And),
    /// Boolean `or`
    Or(Or),
    /// Boolean `xor`
    Xor(Xor),
    /// Boolean `not`
    Not(Not),
    /// The value of the unit type
    Nil(Nil)
}

impl ValueEnum {
    /// Nil
    pub fn nil() -> ValueEnum { ValueEnum::Nil(Nil) }
    /// Logical and
    pub fn land() -> ValueEnum { ValueEnum::And(And) }
    /// Logical or
    pub fn lor() -> ValueEnum { ValueEnum::Or(Or) }
    /// Logical xor
    pub fn lxor() -> ValueEnum { ValueEnum::Xor(Xor) }
    /// Logical not
    pub fn lnot() -> ValueEnum { ValueEnum::Not(Not) }
    /// Get the scope of an iterator over values
    pub fn get_iter_scope<'a, I, V>(iter: I) -> Result<WeakNode<Lambda>, ()>
    where I: Iterator<Item=&'a V>, V: Value + 'a {
        let mut scope = WeakNode::default();
        let mut level = scope.scope_level();
        for val in iter {
            let val_scope = val.get_scope()?;
            let val_level = val_scope.scope_level();
            if val_level > level {
                level = val_level;
                scope = val_scope;
            } else if val_level == level {
                if scope != val_scope { return Err(()) }
            }
        }
        Ok(scope)
    }
}

impl PartialEq for ValueEnum {
    fn eq(&self, other: &ValueEnum) -> bool {
        use ValueEnum::*;
        match self {
            Eval(e) => e.eq(other),
            Lambda(l) => l.eq(other),
            Parameter(p) => p.eq(other),
            Proj(p) => p.eq(other),
            Simple(s) => s.eq(other),
            Tuple(t) => t.eq(other),
            Product(p) => p.eq(other),
            Bool(b) => b.eq(other),
            And(a) => a.eq(other),
            Or(o) => o.eq(other),
            Xor(o) => o.eq(other),
            Not(n) => n.eq(other),
            Nil(n) => n.eq(other)
        }
    }
}

impl Eq for ValueEnum {}

impl Value for ValueEnum {
    #[inline] fn downcast(&self) -> ValueEnum { self.clone() }
    #[inline] fn normalize(&self) -> Result<(), RainError> {
        //TODO
        match self {
            ValueEnum::Eval(e) => e.normalize(),
            _ => Ok(())
        }
    }
    #[inline] fn normalize_mut(&mut self) -> Result<(), RainError> {
        //TODO
        match self {
            ValueEnum::Eval(e) => {
                *self = e.normalized_mut()?;
                Ok(())
            }
            _ => Ok(())
        }
    }
    #[inline] fn normalized(&self) -> Result<ValueEnum, RainError> {
        self.clone().normalized_mut()
    }
    #[inline] fn get_type(&self) -> Option<TypeEnum> {
        use ValueEnum::{Eval, Lambda, Bool, And, Or, Xor, Not, Nil};
        use PrimitiveTy::Bool as Bools;
        use PrimitiveTy::Unit;
        match self {
            Eval(e) => e.get_type(),
            Lambda(l) => l.get_type(),
            Bool(_) => Some(Bools.into()),
            And(_) | Or(_) | Xor(_) =>
                Some(Simple::Binary { domain: (Bools, Bools), target: Bools }.into()),
            Not(_) => Some(Simple::Unary { domain: Bools, target: Bools }.into()),
            Nil(_) => Some(Unit.into()),
            _ => None
        }
    }
    #[inline] fn infer_type(&self) -> Result<Option<TypeEnum>, RainError> {
        match self {
            ValueEnum::Eval(e) => e.infer_type(),
            ValueEnum::Lambda(l) => l.infer_type(),
            _ => Ok(self.get_type())
        }
    }
    #[inline] fn get_scope(&self) -> Result<WeakNode<Lambda>, ()> {
        use ValueEnum::*;
        match self {
            Eval(e) => e.get_scope(),
            Lambda(l) => l.get_scope(),
            Parameter(p) => p.get_scope(),
            Proj(p) => p.get_scope(),
            Simple(s) => s.get_scope(),
            Tuple(t) => t.get_scope(),
            Product(p) => p.get_scope(),
            Bool(b) => b.get_scope(),
            And(a) => a.get_scope(),
            Or(o) => o.get_scope(),
            Xor(x) => x.get_scope(),
            Not(n) => n.get_scope(),
            Nil(n) => n.get_scope()
        }
    }
    #[inline] fn substitute(&self, c: &mut SubstitutionContext)
    -> Result<Option<ValueEnum>, RainError> {
        use ValueEnum::*;
        match self {
            Eval(e) => e.substitute(c),
            Lambda(l) => l.substitute(c),
            Parameter(p) => p.substitute(c),
            Proj(p) => p.substitute(c),
            Simple(s) => s.substitute(c),
            Tuple(t) => t.substitute(c),
            Product(p) => p.substitute(c),
            Bool(b) => b.substitute(c),
            And(a) => a.substitute(c),
            Or(o) => o.substitute(c),
            Xor(x) => x.substitute(c),
            Not(n) => n.substitute(c),
            Nil(n) => n.substitute(c)
        }
    }
}

/// A rain error
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum RainError {
    TypeError,
    NotAFunction,
    InvalidParameter,
    IndexOutOfBounds,
    NotATuple,
    ScopeTooDeep,
    InvalidScope,
    ParamIxOutOfBounds
}

/// The trait implemented by all `rain` values
pub trait Value: Eq + PartialEq<ValueEnum> + Into<ValueEnum> + Clone {
    /// Downcast this value to a `ValueEnum` without normalization
    fn downcast(&self) -> ValueEnum { self.clone().into() }
    /// Normalize this value
    fn normalize(&self) -> Result<(), RainError> { Ok(()) }
    /// Normalize this value, mutating it
    fn normalize_mut(&mut self) -> Result<(), RainError> { Ok(()) }
    /// Downcast this value to a `ValueEnum`, normalized
    fn normalized(&self) -> Result<ValueEnum, RainError> {
        self.normalize()?;
        Ok(self.downcast())
    }
    /// Downcast this value to a `ValueEnum`, normalized mutably
    fn normalized_mut(&mut self) -> Result<ValueEnum, RainError> {
        self.normalize_mut()?;
        Ok(self.downcast())
    }
    /// Get the type of this value, if possible. No normalization.
    #[inline] fn get_type(&self) -> Option<TypeEnum> { None }
    /// Infer the type of this value, if possible
    #[inline] fn infer_type(&self) -> Result<Option<TypeEnum>, RainError>
    { Ok(self.get_type()) }
    /// Infer the type of this value, normalizing
    #[inline] fn infer_type_mut(&mut self) -> Result<Option<TypeEnum>, RainError>
    { self.infer_type() }
    /// Get the scope of this value. Return Err(()) if there are conflicting scopes
    #[inline] fn get_scope(&self) -> Result<WeakNode<Lambda>, ()> { Ok(WeakNode::default()) }
    /// Get the scope level of this value. Return Err(()) if undefined
    #[inline] fn get_scope_level(&self) -> Result<u32, ()> {
        self.get_scope().map(|scope| scope.scope_level())
    }
    /// Evaluate this value within a given scope. Return None if no change.
    #[inline] fn substitute(&self, _c: &mut SubstitutionContext)
    -> Result<Option<ValueEnum>, RainError> { Ok(None) }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::value::ty::PrimitiveTy;
    #[test]
    fn basic_boolean_equality_works() {
        use ValueEnum::*;
        assert_eq!(Bool(true), Bool(true));
        assert_eq!(Bool(false), Bool(false));
        assert_ne!(Bool(true), Bool(false));
        assert_ne!(Bool(false), Bool(true));
        assert_ne!(Bool(true), ValueEnum::land());
    }
    #[test]
    fn bool_is_of_type_bool() {
        use ValueEnum::Bool;
        assert_eq!(Bool(true).get_type().unwrap(), PrimitiveTy::Bool);
        assert_eq!(Bool(false).get_type().unwrap(), PrimitiveTy::Bool);
        assert_eq!(Bool(true).infer_type().unwrap().unwrap(), PrimitiveTy::Bool);
        assert_eq!(Bool(false).infer_type().unwrap().unwrap(), PrimitiveTy::Bool);
    }
}
