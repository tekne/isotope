use std::hash::{Hash, Hasher};
use std::sync::{Arc, Weak};
use std::ops::Deref;
use parking_lot::RwLock;
use super::{Value, ValueEnum, RainError, ty::TypeEnum, lambda::{Lambda, SubstitutionContext}};

/// A `rain` node's data with assSome(* right_val)adata
#[derive(Debug, Clone)]
pub struct NodeData<T> {
    /// The value of this node
    pub val: T,
    /// The type of this node, if one has been assigned
    pub ty: Option<TypeEnum>,
    /// The scope this node has been assigned to, if any, or Err(()) if the scope is undefined
    pub scope: Result<WeakNode<Lambda>, ()>
}

impl<T: PartialEq> PartialEq for NodeData<T> {
    fn eq(&self, other: &NodeData<T>) -> bool {
        (self.ty == other.ty || self.ty.is_none() || other.ty.is_none()) && self.val == other.val
    }
}
impl<T: Eq> Eq for NodeData<T> {}

impl<T: PartialEq<ValueEnum>> PartialEq<ValueEnum> for NodeData<T> {
    fn eq(&self, other: &ValueEnum) -> bool { self.val.eq(other) }
}

impl<T: Value> From<NodeData<T>> for ValueEnum {
    #[inline] fn from(n: NodeData<T>) -> ValueEnum { n.val.into() }
}

impl<T: Value> Value for NodeData<T> {
    fn downcast(&self) -> ValueEnum { self.val.downcast() }
    fn normalize(&self) -> Result<(), RainError> {
        self.val.normalize()?;
        self.ty.as_ref().map(|ty| ty.normalize()).unwrap_or(Ok(()))
    }
    fn normalize_mut(&mut self) -> Result<(), RainError> {
        self.val.normalize_mut()?;
        self.ty.as_mut().map(|ty| ty.normalize_mut()).unwrap_or(Ok(()))
    }
    fn normalized(&self) -> Result<ValueEnum, RainError> {
        self.ty.as_ref().map(|ty| ty.normalize()).unwrap_or(Ok(()))?;
        self.val.normalized()
    }
    fn normalized_mut(&mut self) -> Result<ValueEnum, RainError> {
        self.ty.as_mut().map(|ty| ty.normalize_mut()).unwrap_or(Ok(()))?;
        self.val.normalized_mut()
    }
    fn get_type(&self) -> Option<TypeEnum> { self.ty.clone().or_else(|| self.val.get_type()) }
    fn infer_type(&self) -> Result<Option<TypeEnum>, RainError> {
        if self.ty.is_some() { return Ok(self.ty.clone()) }
        self.val.infer_type()
    }
    fn infer_type_mut(&mut self) -> Result<Option<TypeEnum>, RainError> {
        if self.ty.is_some() { return Ok(self.ty.clone()) }
        let res = self.val.infer_type_mut();
        match res {
            Ok(res) => {
                self.ty = res.clone();
                Ok(res)
            },
            Err(err) => Err(err)
        }
    }
    fn get_scope(&self) -> Result<WeakNode<Lambda>, ()> { self.scope.clone() }
}

impl<T> Deref for NodeData<T> {
    type Target = T;
    fn deref(&self) -> &T { &self.val }
}

/// A `rain` node associated with a given type of data under a lock
#[derive(Debug)]
pub struct Node<T>(Arc<RwLock<NodeData<T>>>);

impl<T> Clone for Node<T> { fn clone(&self) -> Node<T> { Node(self.0.clone()) } }

impl<T, S> PartialEq<S> for Node<T> where NodeData<T>: PartialEq<S> {
    fn eq(&self, other: &S) -> bool { self.0.read().eq(other) }
}
impl<T> PartialEq for Node<T> where T: PartialEq {
    fn eq(&self, other: &Node<T>) -> bool { self.eq(other.0.read().deref()) }
}
impl<T> Eq for Node<T> where T: Eq {}

impl<T> From<NodeData<T>> for Node<T> {
    fn from(data: NodeData<T>) -> Node<T> { Node(Arc::new(RwLock::new(data))) }
}

impl<T> From<Node<T>> for ValueEnum where T: Value {
    #[inline] fn from(n: Node<T>) -> ValueEnum { n.0.read().downcast() }
}
impl<T> Value for Node<T> where T: Value {
    fn downcast(&self) -> ValueEnum { self.0.read().downcast() }
    fn normalize(&self) -> Result<(), RainError> { self.0.write().normalize_mut() }
    fn normalize_mut(&mut self) -> Result<(), RainError> { self.normalize() }
    fn normalized(&self) -> Result<ValueEnum, RainError> { self.0.write().normalized_mut() }
    fn normalized_mut(&mut self) -> Result<ValueEnum, RainError> { self.normalized() }
    fn get_type(&self) -> Option<TypeEnum> { self.0.read().get_type() }
    fn infer_type(&self) -> Result<Option<TypeEnum>, RainError>
    { self.0.write().infer_type_mut() }
    fn infer_type_mut(&mut self) -> Result<Option<TypeEnum>, RainError> { self.infer_type() }
    fn get_scope(&self) -> Result<WeakNode<Lambda>, ()> { self.data().get_scope() }
    fn substitute(&self, c: &mut SubstitutionContext) -> Result<Option<ValueEnum>, RainError> {
        self.data().substitute(c)
    }

}

impl<T> Node<T> {
    pub fn data(&self) -> impl Deref<Target=NodeData<T>> + '_ { self.0.read() }
    pub fn addr(self) -> NodeAddr<T> { NodeAddr(self) }
    pub fn downgrade(&self) -> WeakNode<T> { WeakNode(Arc::downgrade(&self.0)) }
}

/// An address `rain` node associated with a given type of data under a lock
#[derive(Debug)]
pub struct NodeAddr<T>(pub Node<T>);

impl<T> PartialEq for NodeAddr<T> {
    fn eq(&self, other: &NodeAddr<T>) -> bool { Arc::ptr_eq(&(self.0).0, &(other.0).0) }
}
impl<T> Eq for NodeAddr<T> {}

impl<T> PartialEq<ValueEnum> for NodeAddr<T> where Node<T>: PartialEq<ValueEnum> {
    fn eq(&self, other: &ValueEnum) -> bool { self.0.eq(other) }
}

impl<T> Hash for NodeAddr<T> {
    fn hash<H: Hasher>(&self, h: &mut H) {
        let ptr = (self.0).0.as_ref() as *const RwLock<NodeData<T>>;
        h.write_usize(ptr as usize);
    }
}

impl<T> Clone for NodeAddr<T> { fn clone(&self) -> NodeAddr<T> { NodeAddr(self.0.clone()) } }

impl<T> From<NodeAddr<T>> for ValueEnum where ValueEnum: From<Node<T>> {
    #[inline] fn from(a: NodeAddr<T>) -> ValueEnum { ValueEnum::from(a.0) }
}

impl<T> Value for NodeAddr<T> where T: Value {
    fn downcast(&self) -> ValueEnum { self.0.downcast() }
    fn normalize(&self) -> Result<(), RainError> { self.0.normalize() }
    fn normalize_mut(&mut self) -> Result<(), RainError> { self.0.normalize_mut() }
    fn normalized(&self) -> Result<ValueEnum, RainError> { self.0.normalized() }
    fn normalized_mut(&mut self) -> Result<ValueEnum, RainError> { self.0.normalized_mut() }
    fn get_type(&self) -> Option<TypeEnum> { self.0.get_type() }
    fn infer_type(&self) -> Result<Option<TypeEnum>, RainError> { self.0.infer_type() }
    fn infer_type_mut(&mut self) -> Result<Option<TypeEnum>, RainError> { self.0.infer_type_mut() }
    fn get_scope(&self) -> Result<WeakNode<Lambda>, ()> { self.0.get_scope() }
    fn substitute(&self, c: &mut SubstitutionContext) -> Result<Option<ValueEnum>, RainError> {
        self.0.substitute(c)
    }
}

impl<T> NodeAddr<T> {
    /// Get the data associated with this node
    pub fn data(&self) -> impl Deref<Target=NodeData<T>> + '_ { self.0.data() }
    /// Cast this node address to a node
    pub fn node(self) -> Node<T> { self.0 }
}

/// A weak `rain` node associated with a given type of data under a lock
#[derive(Debug)]
pub struct WeakNode<T>(Weak<RwLock<NodeData<T>>>);

impl<T> Clone for WeakNode<T> { fn clone(&self) -> WeakNode<T> { WeakNode(self.0.clone()) } }

impl<T> PartialEq for WeakNode<T> {
    fn eq(&self, other: &WeakNode<T>) -> bool { self.0.as_raw() == other.0.as_raw() }
}
impl<T> Eq for WeakNode<T> {}

impl<T> Hash for WeakNode<T> {
    fn hash<H: Hasher>(&self, h: &mut H) {
        let ptr = self.0.as_raw();
        h.write_usize(ptr as usize);
    }
}

impl<T> Default for WeakNode<T> {
    fn default() -> WeakNode<T> { WeakNode(Weak::default()) }
}

impl<T> WeakNode<T> {
    pub fn upgrade(&self) -> Option<Node<T>> { self.0.upgrade().map(|a| Node(a)) }
    pub fn is_none(&self) -> bool {
        //TODO: this...
        self.0.upgrade().is_none()
    }
}

/*
#[cfg(test)]
mod tests {
    use super::*;
    use crate::value::ty::PrimitiveTy;
    use crate::value::sexpr::Sexpr;
    #[test]
    fn bool_sexpr_type_gets_cached() {
        let mut t = NodeData { val: Sexpr(vec![ValueEnum::Bool(true)]), ty: None };
        let b = || Some(PrimitiveTy::Bool.into());
        assert_eq!(t.infer_type_mut().unwrap(), b());
        assert_eq!(t.ty, b());
    }
}
*/
