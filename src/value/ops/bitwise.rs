use crate::value::{Value, ValueEnum};

/// Bitwise and
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct And;

impl PartialEq<ValueEnum> for And {
    fn eq(&self, other: &ValueEnum) -> bool {
        use ValueEnum::*;
        match other {
            Eval(e) => e.eq(&self.downcast()),
            And(a) => a.eq(self),
            _ => false
        }
    }
}
impl From<And> for ValueEnum { #[inline] fn from(a: And) -> ValueEnum { ValueEnum::And(a) }  }
impl Value for And {}

/// Bitwise or
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct Or;

impl PartialEq<ValueEnum> for Or {
    fn eq(&self, other: &ValueEnum) -> bool {
        use ValueEnum::*;
        match other {
            Eval(e) => e.eq(&self.downcast()),
            Or(o) => o.eq(self),
            _ => false
        }
    }
}
impl From<Or> for ValueEnum { #[inline] fn from(o: Or) -> ValueEnum { ValueEnum::Or(o) }  }
impl Value for Or {}

/// Bitwise xor
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct Xor;

impl PartialEq<ValueEnum> for Xor {
    fn eq(&self, other: &ValueEnum) -> bool {
        use ValueEnum::*;
        match other {
            Eval(e) => e.eq(&self.downcast()),
            Xor(o) => o.eq(self),
            _ => false
        }
    }
}
impl From<Xor> for ValueEnum { #[inline] fn from(x: Xor) -> ValueEnum { ValueEnum::Xor(x) }  }
impl Value for Xor {}

/// Bitwise not
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct Not;

impl PartialEq<ValueEnum> for Not {
    fn eq(&self, other: &ValueEnum) -> bool {
        use ValueEnum::*;
        match other {
            Eval(e) => e.eq(&self.downcast()),
            Not(n) => n.eq(self),
            _ => false
        }
    }
}
impl From<Not> for ValueEnum { #[inline] fn from(n: Not) -> ValueEnum { ValueEnum::Not(n) } }
impl Value for Not {}
