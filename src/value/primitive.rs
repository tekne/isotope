use super::{Value, ValueEnum};

/// The unit rain value
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct Nil;

impl PartialEq<ValueEnum> for Nil {
    fn eq(&self, other: &ValueEnum) -> bool {
        use ValueEnum::*;
        match other {
            Eval(e) => e.eq(&self.downcast()),
            Nil(_) => true,
            _ => false
        }
    }
}
impl From<Nil> for ValueEnum { #[inline] fn from(n: Nil) -> ValueEnum { ValueEnum::Nil(n) } }
impl Value for Nil {}

impl PartialEq<ValueEnum> for bool {
    fn eq(&self, other: &ValueEnum) -> bool {
        use ValueEnum::*;
        match other {
            Eval(e) => e.eq(&self.downcast()),
            Bool(b) => self.eq(b),
            _ => false
        }
    }
}
impl From<bool> for ValueEnum { #[inline] fn from(b: bool) -> ValueEnum { ValueEnum::Bool(b) } }
impl Value for bool {}
