use super::{Value, ValueEnum, RainError, sexpr::Eval, node::WeakNode, lambda::Lambda};

/// A projection of a `rain` value
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Projection {
    pub val: Box<ValueEnum>,
    pub ix: u32
}

impl PartialEq<ValueEnum> for Projection {
    fn eq(&self, other: &ValueEnum) -> bool {
        match other {
            ValueEnum::Eval(e) => e.eq(self),
            ValueEnum::Proj(p) => p.eq(self),
            _ => false
        }
    }
}

impl From<Projection> for ValueEnum {
    fn from(p: Projection) -> ValueEnum {
        //TODO: normalize...
        ValueEnum::Proj(p)
    }
}

impl Value for Projection {
    #[inline] fn get_scope(&self) -> Result<WeakNode<Lambda>, ()> { self.val.get_scope() }
}

impl ValueEnum {
    pub fn proj(&self, ix: u32) -> Result<ValueEnum, RainError> {
        use RainError::*;
        match self {
            ValueEnum::Eval(e) => if e.ix.is_none() {
                Ok(ValueEnum::Eval(Eval {expr: e.expr.clone(), ix: Some(ix)}))
            } else {
                Ok(ValueEnum::Proj(Projection { val: Box::new(self.clone()), ix }))
            },
            ValueEnum::Tuple(t) =>
                t.0.get(ix as usize)
                    .cloned()
                    .ok_or(IndexOutOfBounds),
            ValueEnum::Proj(_) =>
                Ok(ValueEnum::Proj(Projection { val: Box::new(self.clone()), ix })),
            _ => Err(NotATuple)
        }
    }
    /// Project a ValueEnum to a given index even if this is invalid
    pub fn force_proj(&self, ix: u32) -> ValueEnum {
        self.proj(ix)
            .unwrap_or_else(|_| ValueEnum::Proj(Projection { val: Box::new(self.clone()), ix }))
    }
}
