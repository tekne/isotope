use super::{
    Value, ValueEnum, RainError,
    node::{NodeData, Node, WeakNode},
    ty::{Type, TypeEnum, PrimitiveTy},
    tuple::{Tuple, Product},
    projection::Projection,
    lambda::Lambda
};

/// An S-expression
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Sexpr(pub Vec<ValueEnum>);

impl Sexpr {
    /// "Flatten" this S-expression by placing the elements of all head S-expressions at the front
    pub fn flatten(&mut self) {
        match self.0.pop() {
            Some(ValueEnum::Eval(e)) => if let Some(s) = e.as_sexpr() {
                self.0.extend_from_slice(&s.data().0);
                self.flatten()
            },
            Some(o) => self.0.push(o),
            None => {}
        }
    }
    /// Evaluate a unary primitive function
    fn evaluate_unary_primitive(&mut self, f: ValueEnum) -> Result<(ValueEnum, bool), RainError> {
        use ValueEnum::*;
        use RainError::*;
        use PrimitiveTy::Bool as Bools;
        match f {
            o @ Not(_) => {
                match self.0.pop() {
                    Some(Bool(b)) => Ok((Bool(!b), true)),
                    Some(mut v) => {
                        let ty = v.infer_type_mut()?;
                        if match ty { Some(ty) => ty == Bools, None => true } {
                            self.0.push(v);
                            Ok((o, false))
                        } else {
                            self.0.push(v);
                            self.0.push(o);
                            Err(TypeError)
                        }
                    },
                    None => Ok((o, false))
                }
            },
            f => panic!("{:?} is not a unary primitive!", f)
        }
    }
    /// Evaluate a binary primitive function
    fn evaluate_binary_primitive(&mut self, f: ValueEnum) -> Result<(ValueEnum, bool), RainError> {
        use ValueEnum::*;
        use RainError::*;
        use PrimitiveTy::Bool as Bools;
        match f {
            o @ And(_) | o @ Or(_) | o @ Xor(_) => {
                match self.0.pop() {
                    Some(Bool(a)) => match self.0.pop() {
                        Some(Bool(b)) => {
                            let res = match o {
                                And(_) => a & b,
                                Or(_) => a | b,
                                Xor(_) => a ^ b,
                                _ => unreachable!()
                            };
                            Ok((Bool(res), true))
                        },
                        Some(mut v) => {
                            let ty = v.infer_type_mut()?;
                            if match ty { Some(ty) => ty == Bools, None => true } {
                                self.0.push(v);
                                self.0.push(Bool(a));
                                Ok((o, false))
                            } else {
                                self.0.push(v);
                                self.0.push(Bool(a));
                                self.0.push(o);
                                Err(TypeError)
                            }
                        },
                        None => {
                            self.0.push(Bool(a));
                            Ok((o, false))
                        }
                    },
                    Some(mut v) => {
                        let ty = v.infer_type_mut()?;
                        if match ty { Some(ty) => ty == Bools, None => true } {
                            self.0.push(v);
                            Ok((o, false))
                        } else {
                            self.0.push(v);
                            self.0.push(o);
                            Err(TypeError)
                        }
                    },
                    None => Ok((o, false))
                }
            },
            f => panic!("{:?} is not a binary primitive", f)
        }
    }
    /// Evaluate a lambda function
    fn evaluate_lambda(&mut self, l: Node<Lambda>) -> Result<(ValueEnum, bool), RainError> {
        let data = l.data();
        //TODO: type check, optimize
        if data.params().len() > self.0.len() {
            Ok((ValueEnum::Lambda(l.clone()), false))
        } else {
            let ix = self.0.len() - data.params().len();
            let res = match l.evaluate(&self.0[ix..]) {
                Ok(res) => res,
                Err(err) => {
                    self.0.push(ValueEnum::Lambda(l.clone()));
                    return Err(err)
                }
            };
            self.0.truncate(ix);
            Ok((res, true))
        }
    }
    /// Attempt to evaluate this S-expression assuming it is already elementwise normalized
    pub fn evaluate(&mut self) -> Result<(), RainError> {
        use ValueEnum::*;
        use RainError::*;
        if self.0.len() <= 1 { return Ok(()) }
        let f = self.0.pop().unwrap();
        let (res, eval_happened) = match f {
            o @ And(_) | o @ Or(_) | o @ Xor(_) => self.evaluate_binary_primitive(o)?,
            o @ Not(_) => self.evaluate_unary_primitive(o)?,
            Lambda(l) => self.evaluate_lambda(l)?,
            mut v => {
                let ty = v.infer_type_mut()?;
                match ty {
                    Some(ty) if ty.is_function() => {
                        // Later check argument types, for now do nothing
                        (v, false)
                    }
                    None => {
                        // Do nothing
                        (v, false)
                    },
                    _ => {
                        self.0.push(v);
                        return Err(NotAFunction)
                    }
                }
            }
        };
        self.0.push(res);
        if eval_happened { self.evaluate() } else { Ok(()) }
    }
}

impl PartialEq<ValueEnum> for Sexpr {
    fn eq(&self, other: &ValueEnum) -> bool {
        let res = match other {
            ValueEnum::Nil(_) => self.0.len() == 0,
            ValueEnum::Eval(e) => e.ix.is_none() && self.eq(&e.expr.data().val),
            _ => false
        };
        res || (self.0.len() == 1 && self.0[0].eq(other))
    }
}

impl PartialEq<Tuple> for Sexpr {
    fn eq(&self, other: &Tuple) -> bool {
        self.0.len() == 1 && other.eq(&self.0[0])
    }
}

impl PartialEq<Product> for Sexpr {
    fn eq(&self, other: &Product) -> bool {
        self.0.len() == 1 && other.eq(&self.0[0])
    }
}

impl PartialEq<Projection> for Sexpr {
    fn eq(&self, other: &Projection) -> bool {
        self.0.len() == 1 && other.eq(&self.0[0])
    }
}

impl From<Sexpr> for NodeData<Sexpr> {
    fn from(val: Sexpr) -> NodeData<Sexpr> {
        let ty = val.get_type();
        let scope = val.get_scope();
        NodeData { val, ty, scope }
    }
}

impl From<Sexpr> for ValueEnum {
    fn from(mut val: Sexpr) -> ValueEnum {
        if val.0.len() == 0 { return ValueEnum::nil() }
        if val.0.len() == 1 { return val.0.pop().unwrap().into() }
        ValueEnum::Eval(Node::from(NodeData::from(val.clone())).into())
    }
}

impl Value for Sexpr {
    fn downcast(&self) -> ValueEnum {
        if self.0.len() == 0 { return ValueEnum::nil() }
        if self.0.len() == 1 { return self.0[0].downcast() }
        ValueEnum::Eval(Node::from(NodeData::from(self.clone())).into())
    }
    fn normalize(&self) -> Result<(), RainError> {
        for elem in self.0.iter() { elem.normalize()? }
        Ok(())
    }
    fn normalize_mut(&mut self) -> Result<(), RainError> {
        // Step 1: componentwise normalization
        for elem in self.0.iter_mut() { elem.normalize_mut()? }
        // Step 2: flattening
        self.flatten();
        // Step 3: evaluation
        self.evaluate()
    }
    fn normalized(&self) -> Result<ValueEnum, RainError> {
        let mut cloned = self.clone();
        cloned.normalized_mut()
    }
    fn get_type(&self) -> Option<TypeEnum> {
        if self.0.len() == 0 { return Some(PrimitiveTy::Unit.into()) }
        else if self.0.len() == 1 { return self.0[0].get_type() }
        //TODO: evaluation type
        None
    }
    fn infer_type(&self) -> Result<Option<TypeEnum>, RainError> {
        if self.0.len() == 0 { return Ok(Some(PrimitiveTy::Unit.into())) }
        else if self.0.len() == 1 { return self.0[0].infer_type() }
        //TODO: evaluation type
        Ok(None)
    }
    fn infer_type_mut(&mut self) -> Result<Option<TypeEnum>, RainError> {
        self.normalize_mut()?;
        if self.0.len() == 0 { return Ok(Some(PrimitiveTy::Unit.into())) }
        if self.0.len() == 1 { return self.0[0].infer_type_mut() }
        Ok(None)
    }
    fn get_scope(&self) -> Result<WeakNode<Lambda>, ()> {
        ValueEnum::get_iter_scope(self.0.iter())
    }
}

/// An evaluation of an S-expression, often associated with a projection
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Eval {
    /// The index to project the result of this evaluation to, if any
    pub ix: Option<u32>,
    /// This evaluation
    pub expr: Node<Sexpr>
}

impl Eval {
    /// Get this evaluation as an S-expression if possible`
    #[inline] pub fn as_sexpr(&self) -> Option<&Node<Sexpr>> {
        if self.ix.is_none() { Some(&self.expr) } else { None }
    }
}

impl From<Node<Sexpr>> for Eval {
    #[inline] fn from(expr: Node<Sexpr>) -> Eval { Eval { ix: None, expr } }
}

impl PartialEq<ValueEnum> for Eval {
    fn eq(&self, other: &ValueEnum) -> bool {
        match other {
            ValueEnum::Eval(e) => e.eq(self),
            v => if self.ix.is_none() { self.expr.eq(v) } else { false }
        }
    }
}

impl From<Eval> for ValueEnum {
    #[inline] fn from(e: Eval) -> ValueEnum {
        let v = ValueEnum::from(e.expr);
        match e.ix {
            Some(p) => v.force_proj(p),
            None => v
        }
    }
}

impl PartialEq<Product> for Eval {
    fn eq(&self, other: &Product) -> bool {
        self.ix.is_none() && self.expr.data().val.eq(other)
    }
}

impl PartialEq<Tuple> for Eval {
    fn eq(&self, other: &Tuple) -> bool {
        self.ix.is_none() && self.expr.data().val.eq(other)
    }
}

impl PartialEq<Projection> for Eval {
    fn eq(&self, other: &Projection) -> bool {
        match self.ix {
            Some(p) => p == other.ix && self.expr.eq(other.val.as_ref()),
            None => self.expr.data().val.eq(other)
        }
    }
}

impl Value for Eval {
    #[inline] fn downcast(&self) -> ValueEnum {
        let v = self.expr.downcast();
        match self.ix {
            Some(p) => v.force_proj(p),
            None => v
        }
    }
    #[inline] fn normalize(&self) -> Result<(), RainError> { self.expr.normalize() }
    #[inline] fn normalize_mut(&mut self) -> Result<(), RainError> { self.expr.normalize_mut() }
    #[inline] fn normalized(&self) -> Result<ValueEnum, RainError> {
        let v = self.expr.normalized()?;
        match self.ix {
            Some(p) => v.proj(p),
            None => Ok(v)
        }
    }
    #[inline] fn normalized_mut(&mut self) -> Result<ValueEnum, RainError> {
        let v = self.expr.normalized_mut()?;
        match self.ix {
            Some(p) => v.proj(p),
            None => Ok(v)
        }
    }
    #[inline] fn get_scope(&self) -> Result<WeakNode<Lambda>, ()> {
        self.expr.get_scope()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::value::ops::bitwise::*;
    use crate::value::Nil;
    use ValueEnum::Bool;
    use RainError::*;
    fn t() -> ValueEnum { Bool(true) }
    fn f() -> ValueEnum { Bool(false) }
    fn b() -> TypeEnum { PrimitiveTy::Bool.into() }
    fn u() -> TypeEnum { PrimitiveTy::Unit.into() }
    fn and() -> ValueEnum { ValueEnum::And(And) }
    fn or() -> ValueEnum { ValueEnum::Or(Or) }
    fn xor() -> ValueEnum { ValueEnum::Xor(Xor) }
    fn not() -> ValueEnum { ValueEnum::Not(Not) }
    fn nil() -> ValueEnum { ValueEnum::Nil(Nil) }

    #[test]
    fn sexpr_nested_bool_equality_works() {
        let t = Sexpr(vec![Bool(true)]);
        let f = Sexpr(vec![Bool(false)]);
        assert_eq!(t, Bool(true));
        assert_ne!(t, Bool(false));
        assert_eq!(f, Bool(false));
        assert_ne!(f, Bool(true));
    }
    #[test]
    fn sexpr_nested_bool_downcast_works() {
        let t = Sexpr(vec![Bool(true)]);
        let f = Sexpr(vec![Bool(false)]);
        assert_eq!(t.downcast(), Bool(true));
        assert_eq!(f.downcast(), Bool(false));
        let t2 = Sexpr(vec![Bool(false), or()]);
        assert_eq!(t2, t2.downcast());
    }
    #[test]
    fn sexpr_nested_bool_normalization_works() {
        let t = Sexpr(vec![Bool(true)]);
        let f = Sexpr(vec![Bool(false)]);
        assert_eq!(t.normalized().unwrap(), Bool(true));
        assert_eq!(f.normalized().unwrap(), Bool(false));
        let t2 = Sexpr(vec![Bool(false), or()]);
        assert_eq!(t2, t2.normalized().unwrap());
    }
    #[test]
    fn binary_sexpr_flattening_works() {
        let mut f = Sexpr(vec![Bool(false), Sexpr(vec![Bool(true), and()]).downcast()]);
        let ff = Sexpr(vec![Bool(false), Bool(true), and()]);
        f.flatten();
        assert_eq!(f, ff);
        assert_eq!(f.0, ff.0);
    }
    #[test]
    fn bool_unary_ops_normalize_properly() {
        assert_eq!(Sexpr(vec![Bool(true), not()]).normalized_mut().unwrap(), Bool(false));
        assert_eq!(Sexpr(vec![Bool(false), not()]).normalized_mut().unwrap(), Bool(true));
    }
    #[test]
    fn bool_binary_ops_normalize_properly() {
        assert_eq!(Sexpr(vec![t(), t(), and()]).normalized_mut().unwrap(), t());
        assert_eq!(Sexpr(vec![t(), f(), and()]).normalized_mut().unwrap(), f());
        assert_eq!(Sexpr(vec![f(), t(), and()]).normalized_mut().unwrap(), f());
        assert_eq!(Sexpr(vec![f(), f(), and()]).normalized_mut().unwrap(), f());
        assert_eq!(Sexpr(vec![t(), t(), or()]).normalized_mut().unwrap(), t());
        assert_eq!(Sexpr(vec![t(), f(), or()]).normalized_mut().unwrap(), t());
        assert_eq!(Sexpr(vec![f(), t(), or()]).normalized_mut().unwrap(), t());
        assert_eq!(Sexpr(vec![f(), f(), or()]).normalized_mut().unwrap(), f());
        assert_eq!(Sexpr(vec![t(), t(), xor()]).normalized_mut().unwrap(), f());
        assert_eq!(Sexpr(vec![t(), f(), xor()]).normalized_mut().unwrap(), t());
        assert_eq!(Sexpr(vec![f(), t(), xor()]).normalized_mut().unwrap(), t());
        assert_eq!(Sexpr(vec![f(), f(), xor()]).normalized_mut().unwrap(), f());
    }
    #[test]
    fn bool_function_on_bool_function_gives_type_error() {
        let s = Sexpr(vec![and(), and()]);
        let mut sc = s.clone();
        assert_eq!(sc.normalized_mut(), Err(TypeError));
        assert_eq!(s, sc);
    }
    #[test]
    fn empty_sexpr_typing_works() {
        let e = Sexpr(vec![]);
        let mut em = e.clone();
        let u = Some(u());
        assert_eq!(e.get_type(), u);
        let u = Ok(u);
        assert_eq!(e.infer_type(), u);
        assert_eq!(em.infer_type_mut(), u);
        assert_eq!(e, em);
    }
    #[test]
    fn sexpr_nested_bool_get_type_works() {
        let t = Sexpr(vec![Bool(true)]);
        let f = Sexpr(vec![Bool(false)]);
        let b = Some(b());
        assert_eq!(t.get_type(), b);
        assert_eq!(f.get_type(), b);
    }
    #[test]
    fn sexpr_nested_bool_infer_type_works() {
        let t = Sexpr(vec![Bool(true)]);
        let f = Sexpr(vec![Bool(false)]);
        let b = Ok(Some(b()));
        assert_eq!(t.infer_type(), b);
        assert_eq!(f.infer_type(), b);
    }
    #[test]
    fn sexpr_nested_bool_infer_type_mut_works() {
        let t = Sexpr(vec![Bool(true)]);
        let mut tm = t.clone();
        let f = Sexpr(vec![Bool(false)]);
        let mut fm = f.clone();
        let b = Ok(Some(b()));
        assert_eq!(tm.infer_type_mut(), b);
        assert_eq!(fm.infer_type_mut(), b);
        assert_eq!(t, tm);
        assert_eq!(f, fm);
    }
    #[test]
    fn nil_empty_sexpr_equality_works() {
        let e = Sexpr(vec![]);
        assert_eq!(e, nil());
        match e.downcast() {
            ValueEnum::Nil(_) => {},
            bad => panic!("Downcasting the empty sexpr should yield nil, got {:?}", bad)
        }
    }
    #[test]
    fn nil_is_not_a_function() {
        assert_eq!(Sexpr(vec![nil(), nil()]).normalize_mut(), Err(NotAFunction))
    }
}
