use super::{Value, ValueEnum, ty::TypeEnum, node::WeakNode, lambda::Lambda};
use thincollections::thin_vec::ThinVec;

/// A tuple of values
#[derive(Debug, Clone, PartialEq)]
pub struct Tuple(pub ThinVec<ValueEnum>);

impl Eq for Tuple {}

impl PartialEq<ValueEnum> for Tuple {
    fn eq(&self, other: &ValueEnum) -> bool {
        match other {
            ValueEnum::Eval(e) => e.eq(self),
            ValueEnum::Tuple(t) => t.eq(self),
            _ => false
        }
    }
}
impl From<Tuple> for ValueEnum { #[inline] fn from(t: Tuple) -> ValueEnum { ValueEnum::Tuple(t) } }
impl Value for Tuple {
    #[inline] fn get_scope(&self) -> Result<WeakNode<Lambda>, ()> {
        ValueEnum::get_iter_scope(self.0.iter())
    }
}

/// A product type
#[derive(Debug, Clone, PartialEq)]
pub struct Product(pub ThinVec<TypeEnum>);

impl Eq for Product {}

impl PartialEq<ValueEnum> for Product {
    fn eq(&self, other: &ValueEnum) -> bool {
        match other {
            ValueEnum::Eval(e) => e.eq(self),
            ValueEnum::Product(p) => p.eq(self),
            _ => false
        }
    }
}
impl From<Product> for ValueEnum {
    #[inline] fn from(p: Product) -> ValueEnum { ValueEnum::Product(p) }
}
impl Value for Product {
    #[inline] fn get_scope(&self) -> Result<WeakNode<Lambda>, ()> {
        ValueEnum::get_iter_scope(self.0.iter())
    }
}
