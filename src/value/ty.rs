use super::{Value, ValueEnum};

/// A `rain` value known to be a type
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum TypeEnum {
    Simple(Simple)
}

pub trait Type: Value {
    /// Whether this type is a function type
    fn is_function(&self) -> bool;
}

impl Type for TypeEnum {
    fn is_function(&self) -> bool {
        match self {
            TypeEnum::Simple(s) => s.is_function()
        }
    }
}

impl From<Simple> for TypeEnum {
    fn from(ty: Simple) -> TypeEnum { TypeEnum::Simple(ty) }
}

impl From<PrimitiveTy> for TypeEnum {
    fn from(ty: PrimitiveTy) -> TypeEnum { TypeEnum::Simple(Simple::Primitive(ty)) }
}

impl PartialEq<ValueEnum> for TypeEnum {
    fn eq(&self, other: &ValueEnum) -> bool {
        match self {
            TypeEnum::Simple(p) => p.eq(other)
        }
    }
}

impl PartialEq<PrimitiveTy> for TypeEnum {
    fn eq(&self, other: &PrimitiveTy) -> bool {
        match self {
            TypeEnum::Simple(p) => p.eq(other)
        }
    }
}
impl From<TypeEnum> for ValueEnum {
    fn from(t: TypeEnum) -> ValueEnum {
        match t {
            TypeEnum::Simple(s) => s.into()
        }
    }
}
impl Value for TypeEnum {
    fn downcast(&self) -> ValueEnum {
        match self {
            TypeEnum::Simple(s) => s.downcast()
        }
    }
}

/// A simple `rain` type
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum Simple {
    /// A primitive type
    Primitive(PrimitiveTy),
    /// A function from a primitive type to a primitive type
    Unary { domain: PrimitiveTy, target: PrimitiveTy },
    /// A function from two primitive types to a primitive type
    Binary { domain: (PrimitiveTy, PrimitiveTy), target: PrimitiveTy }
}

impl Type for Simple {
    fn is_function(&self) -> bool {
        match self {
            Simple::Primitive(_) => false,
            _ => true
        }
    }
}

impl PartialEq<PrimitiveTy> for Simple {
    fn eq(&self, other: &PrimitiveTy) -> bool {
        match self {
            Self::Primitive(p) => p.eq(other),
            _ => false
        }
    }
}

impl PartialEq<ValueEnum> for Simple {
    fn eq(&self, other: &ValueEnum) -> bool {
        match other {
            ValueEnum::Simple(p) => p.eq(self),
            _ => false // TODO: sexprs
        }
    }
}
impl From<Simple> for ValueEnum {
    #[inline] fn from(s: Simple) -> ValueEnum { ValueEnum::Simple(s) }
}
impl Value for Simple {}

/// A primitive `rain` type
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum PrimitiveTy {
    /// The type of Boolean values
    Bool,
    /// The unit type
    Unit
}

impl Type for PrimitiveTy { fn is_function(&self) -> bool { false } }

impl PartialEq<ValueEnum> for PrimitiveTy {
    fn eq(&self, other: &ValueEnum) -> bool {
        match other {
            ValueEnum::Simple(p) => p.eq(self),
            _ => false //TODO: sexprs
        }
    }
}
impl From<PrimitiveTy> for ValueEnum {
    #[inline] fn from(p: PrimitiveTy) -> ValueEnum {
        ValueEnum::Simple(Simple::Primitive(p))
    }
}
impl Value for PrimitiveTy {}
